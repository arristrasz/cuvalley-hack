import keras
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler, MinMaxScaler, Normalizer
from sklearn.model_selection import train_test_split


csv_path = 'C:/Users/Arristrasz/Desktop/Uczelnia/Projekty/dane/20200525.csv'
raw_data = pd.read_csv(csv_path)
raw_data = raw_data.to_dict('index')

current_dictionary = {}
encoder_dictionary = {}
time_dictionary = {}
direction_dictionary = {}

hehs = {}
# Usuwanie rekordów ze słownika, gdzie dict['direction] == 0
#hehs = {x for x in raw_data if x['direction'] == 1}

for row in raw_data:
    if raw_data[row]['counter'] in current_dictionary.keys():
        current_dictionary[raw_data[row]['counter']].append(raw_data[row]['current'])
        encoder_dictionary[raw_data[row]['counter']].append(raw_data[row]['encoder'])
        time_dictionary[raw_data[row]['counter']].append(raw_data[row]['time'])
        direction_dictionary[raw_data[row]['counter']].append(raw_data[row]['direction'])
    else:
        current_dictionary[raw_data[row]['counter']] = [raw_data[row]['current']]
        encoder_dictionary[raw_data[row]['counter']] = [raw_data[row]['encoder']]
        time_dictionary[raw_data[row]['counter']] = [raw_data[row]['time']]
        direction_dictionary[raw_data[row]['counter']] = [raw_data[row]['direction']]

direction = [row[0] for row in direction_dictionary.values()]

time_dictionary_scale = {}
for key in time_dictionary:
    time_dictionary_scale[key] = [time-time_dictionary[key][0] for time in time_dictionary[key]]

time_dictionary_scale_delta = {}
for key in time_dictionary_scale:
    time_dictionary_scale_delta[key] = [y - x for x, y in zip(time_dictionary_scale[key], time_dictionary_scale[key][1:])]

input_features = {}
for key in time_dictionary_scale:
    input_features[key] = current_dictionary[key][:]
    # input_features[key] = time_dictionary_scale_delta[key][:]
    # input_features[key].extend(current_dictionary[key])

xdd = list(map(bool, direction))
xdd = [not i for i in xdd]
input_features = list(input_features.values())
result = [x for i, x in enumerate(input_features) if xdd[i]]
#input_features = dict(result)
input_features = result

for i in range(int(len(input_features))):
    plt.plot(input_features[i], c='red')
plt.show()

x_train, x_test, y_train, y_test = train_test_split(input_features, input_features, test_size=0.20, random_state=42)

#scaler = Normalizer()
scaler = StandardScaler()
scaler.fit(x_train)

x_train = scaler.transform(x_train)
x_test = scaler.transform(x_test)

scaler2 = MinMaxScaler()
scaler2.fit(x_train)

x_train = scaler2.transform(x_train)
x_test = scaler2.transform(x_test)

############################################################



"""
HALO
Problem jest pewnie w tym, że ten list of boolean, ktory ejst do dobierania klas 
się rozjeżdża po fukcji, ktora robi train-test split i dlatego bierze mi jakieś gowna
"""





############################################################

#for i in range(int(len(encoder_dictionary)/5)):
#    plt.scatter(range(len(x_train[i])), x_train[i], c='blue')
#plt.show()


input_data_dim = (len(x_train[0]))
encoding_neurons = 2

input_vector = keras.layers.Input(shape=(input_data_dim,))
encoded = keras.layers.Dense(encoding_neurons, activation='relu')(input_vector)
decoded = keras.layers.Dense(input_data_dim, activation='sigmoid')(encoded)

auto_encoder = keras.models.Model(input_vector, decoded)
encoder = keras.models.Model(input_vector, encoded)

encoded_input = keras.layers.Input(shape=(encoding_neurons,))
decoder_layer = auto_encoder.layers[-1]

decoder = keras.models.Model(encoded_input, decoder_layer(encoded_input))

auto_encoder.compile(optimizer='sgd', loss='mse')
#auto_encoder.compile(optimizer='adam', loss='binary_crossentropy')

auto_encoder.fit(x_train, x_train,
                 epochs=50,
                 batch_size=1,
                 shuffle=True,
                 validation_data=(x_test, x_test))

encoded_imgs = encoder.predict(x_test)
decoded_imgs = decoder.predict(encoded_imgs)


for i in range(int(len(input_features)/5)):
    plt.scatter(range(len(decoded_imgs[i])), decoded_imgs[i]-x_test[i], c='red')
plt.show()

#losowanie 10 najgorszych
all_readings = np.append(x_train, x_test, axis=0)
#all_readings = input_features
xd = auto_encoder.predict(all_readings)

abs_vals = abs(xd-all_readings)
maximizer = [np.sum(x) for x in abs_vals]
index0 = np.argmax(maximizer)

abs_vals = abs(xd-all_readings)
abs_vals[index0] = 0
maximizer = [np.sum(x) for x in abs_vals]
index1 = np.argmax(maximizer)

abs_vals = abs(xd-all_readings)
abs_vals[index0] = 0
abs_vals[index1] = 0
maximizer = [np.sum(x) for x in abs_vals]
index2 = np.argmax(maximizer)


#max_vals = np.max(abs_vals)

"""
xd[index] = 0
all_readings[index] = 0
index2 = np.argmax(np.mean(abs(xd-all_readings)))
xd[index2] = 0
all_readings[index2] = 0
index3 = np.argmax(np.mean(abs(xd-all_readings)))
xd[index3] = 0
all_readings[index3] = 0
index4 = np.argmax(np.mean(abs(xd-all_readings)))
xd[index4] = 0
all_readings[index4] = 0
"""


xdd = list(map(bool, direction))
xdd = [not i for i in xdd]
current_dictionary = list(current_dictionary.values())
result = [x for i, x in enumerate(current_dictionary) if xdd[i]]
#input_features = dict(result)
current_dictionary = result

# Plot wartości anomalnej
for i in range(len(list(all_readings))):
    plt.plot(list(scaler.inverse_transform(scaler2.inverse_transform(all_readings)))[i], c='grey')

plt.plot(list(scaler.inverse_transform(scaler2.inverse_transform(all_readings)))[index0], c='red')
plt.plot(list(scaler.inverse_transform(scaler2.inverse_transform(all_readings)))[index1], c='red')
plt.plot(list(scaler.inverse_transform(scaler2.inverse_transform(all_readings)))[index2], c='red')
plt.xlabel("Turnpikes' angular position")
plt.ylabel("Turnpikes' motor current value")
plt.show()

# Sredni blad
for i in range(int(len(xd))):
    plt.scatter(i, np.mean(abs(xd[i]-all_readings[i])), c='grey')

plt.scatter(index0, np.mean(abs(xd[index0]-all_readings[index0])), c='red')
plt.scatter(index1, np.mean(abs(xd[index1]-all_readings[index1])), c='red')
plt.scatter(index2, np.mean(abs(xd[index2]-all_readings[index2])), c='red')

plt.xlabel('Index of evaluated current waveform')
plt.ylabel('Mean encoding error for all neurons')
plt.show()

# Max blad
for i in range(int(len(xd))):
    plt.scatter(i, np.max(abs(xd[i]-all_readings[i])), c='grey')

plt.scatter(index0, np.mean(abs(xd[index0]-all_readings[index0])), c='red')
plt.scatter(index1, np.mean(abs(xd[index1]-all_readings[index1])), c='red')
plt.scatter(index2, np.mean(abs(xd[index2]-all_readings[index2])), c='red')

plt.xlabel('Index of evaluated current waveform')
plt.ylabel('Mean encoding error for all neurons')
plt.show()

# Neuron 1 i 2
for i in range(int(len(input_features)/5)):
    plt.scatter(encoded_imgs[i][0], encoded_imgs[i][1], c='red')
plt.xlabel('Encoder layer first neuron value')
plt.ylabel('Encoder layer second neuron value')
plt.show()
