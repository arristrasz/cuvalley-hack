# -*- coding: utf-8 -*-
"""
Created on Sat Jun 12 14:09:16 2021

@author: bgren
"""

from influxdb_client import InfluxDBClient, Point, WriteOptions
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb import DataFrameClient
from influxdb import InfluxDBClient

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime 
import time
import zipfile
import sys
import os

from collections import namedtuple

#engine failures

"""
tags_suffices = [r'BREAKP_____AkPa',
r'ENGCOOLT___A__C',
r'ENGHOURS___A__h',
r'ENGOILP____AkPa',
r'ENGOILT____A__C',
r'ENGRPM_____Arpm',
r'ENGTPS_____A__%',
r'FUELUS_____Al_h',
r'GROILP_____AkPa',
r'GROILT_____A__C',
r'HYDOILP____AMPa',
r'HYDOILT____A__C',
r'INTAKEP____AkPa',
r'INTAKET____A__C',
r'SELGEAR____A_/_',
r'SPEED______Akmh',
r'TEMPIN_____A__C',
r'TRNAUT_____D___']
"""

engine_failures_info = [
    ['WOS_175L', r'WOS___175L', r'2021-02-11T00:00:00+00:00', 14*24],
    ['WOS_174L', r'WOS___174L', r'none', 14*24],
    ['WOS_176L', r'WOS___176L', r'none', 14*24],
    ['WOS_177L', r'WOS___177L', r'none', 14*24],
    ['WOS_179L', r'WOS___179L', r'none', 14*24],
    ['LK3_045L', r'LK3___045L', r'2021-04-04T00:00:00+00:00', 14*24],
    ['LK3_046L', r'LK3___046L', r'none', 14*24],
    ['LK3_048L', r'LK3___048L', r'none', 14*24],
    ['LK3_050L', r'LK3___050L', r'2020-05-20T00:00:00+00:00', 14*24],    
]

gearbox_failures_info = [
    ['WOS_175L', r'WOS___175L', r'none', 14*24],
    ['WOS_174L', r'WOS___174L', r'2020-10-03T00:00:00+00:00', 14*24],
    ['WOS_176L', r'WOS___176L', r'2021-01-29T00:00:00+00:00', 14*24],
    ['WOS_177L', r'WOS___177L', r'2021-02-27T00:00:00+00:00', 14*24],
    ['WOS_179L', r'WOS___179L', r'2021-05-06T00:00:00+00:00', 14*24],
    ['LK3_045L', r'LK3___045L', r'none', 14*24],
    ['LK3_046L', r'LK3___046L', r'2020-04-15T00:00:00+00:00', 14*24],
    ['LK3_048L', r'LK3___048L', r'2021-04-14T00:00:00+00:00', 14*24],
    ['LK3_050L', r'LK3___050L', r'2021-01-17T00:00:00+00:00', 14*24],    
]


e1 = '1546365444000ms' #2019.01.01
e2 = '1622570244000ms' #2021.06.01

client = DataFrameClient(host='127.0.0.1', port=8086, database='cuvalley2')
folder = r'D:\00_cuvalley'
file_suffix = 'gbx_gear_1'

objects_info = engine_failures_info

#################
#objects_info = [objects_info[0]]

for x in objects_info:
    failure_time = x[2]
    tag_part = x[1]
    db = x[0]
    file = tag_part + '_' + file_suffix + '.csv'
    path_out = os.path.join(folder,file)
    horizon = x[3]
    
    # tylko na drugim biegu
    gearbox_query_data = [
    r'max("KLDSMG_' + tag_part + r'ENGCOOLT___A__C") AS "T_cool_max", ',
    r'max("KLDSMG_'+ tag_part + r'ENGOILP____AkPa") AS "P_oil_max", ',
    r'max("KLDSMG_'+ tag_part + r'ENGRPM_____Arpm") AS "n_eng_max", ',
    r'mean("KLDSMG_'+ tag_part + r'ENGRPM_____Arpm") AS "n_eng_avg", ',
    r'max("KLDSMG_'+ tag_part + r'SPEED______Akmh") AS "v_max", ',
    r'mean("KLDSMG_'+ tag_part + r'SPEED______Akmh") AS "v_avg", ',
    r'max("KLDSMG_'+ tag_part + r'HYDOILP____AMPa") AS "p_hyd_max", ',
    r'mean("KLDSMG_'+ tag_part + r'HYDOILP____AMPa") AS "p_hyd_avg", ',
    r'max("KLDSMG_'+ tag_part + r'HYDOILT____A__C") AS "T_hyd_max", ',
    r'mean("KLDSMG_'+ tag_part + r'HYDOILT____A__") AS "T_hyd_avg" ',
    r'FROM "autogen"."' + db +'" ',
    r'WHERE ("KLDSMG_'+ tag_part + r'SELGEAR____A_/_" > 0 ',
    r'AND "KLDSMG_'+ tag_part + r'SELGEAR____A_/_" < 2) ',
    r'AND time >= ' + e1 + r' and time <= ' + e2+ ' ',
    r'GROUP BY time(1h);']# epoch=ms']

    # tylko na 1szym biegu
    engine_query_data = [
    r'max("KLDSMG_' + tag_part + r'GROILP_____AkPa") AS "P_gb_max", ',
    r'max("KLDSMG_'+ tag_part + r'GROILT_____A__C") AS "T_gb_max", ',
    r'max("KLDSMG_'+ tag_part + r'ENGRPM_____Arpm") AS "n_eng_max", ',
    r'mean("KLDSMG_'+ tag_part + r'ENGRPM_____Arpm") AS "n_eng_avg", ',
    r'mean("KLDSMG_'+ tag_part + r'FUELUS_____Al_h") AS "usage_avg", ',
    r'mean("KLDSMG_'+ tag_part + r'SPEED______Akmh") AS "v_avg", ',
    r'max("KLDSMG_'+ tag_part + r'ENGHOURS___A__h") AS "work_h_max", ',
    r'mean("KLDSMG_'+ tag_part + r'INTAKEP____AkPa") AS "intk_p_avg", ',
    r'max("KLDSMG_'+ tag_part + r'INTAKEP____AkPa") AS "intk_p_max", ',
    r'mean("KLDSMG_'+ tag_part + r'INTAKET____A__C") AS "intk_t_avg", ',
    r'max("KLDSMG_'+ tag_part + r'INTAKET____A__C") AS "intk_t_max" ',
    r'FROM "autogen"."' + db +'" ',
    r'WHERE ("KLDSMG_'+ tag_part + r'SELGEAR____A_/_" > 0 ',
    r'AND "KLDSMG_'+ tag_part + r'SELGEAR____A_/_" < 2) ',
    r'AND time >= ' + e1 + r' and time <= ' + e2+ ' ',
    r'GROUP BY time(1h);']

    ##print(len(tmp))
    #print(type(tmp))
    #print(tmp[0])
    tmp = gearbox_query_data
    qr = str(r'SELECT ')
    for t in tmp:
        #print(t)
    # print(qr)
        qr = qr + t

    #print(qr)
    out = client.query(query=qr)
    #print(out)
    df = out[db]
    #print(df.head)
    df['Time'] = df.index
    #print(df.head)
    df.reset_index(inplace=True, drop = True)
    print(df.head)
    
   
    df['failure'] = [0] * len(df.index)
    if failure_time != 'none':
        i = df.loc[df['Time'] == failure_time].index[0]
        print(i)
        df.loc[i-horizon:i,'failure'] = 1

    #print(df.columns)
    df = df.dropna()
    df.to_csv(path_out)
