# -*- coding: utf-8 -*-
"""
Created on Sun Jun 13 02:50:24 2021

@author: bgren
"""

from influxdb_client import InfluxDBClient, Point, WriteOptions
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb import DataFrameClient
from influxdb import InfluxDBClient

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import datetime 
import time
import zipfile
import sys
import os

h = pd.read_csv(r'D:\00_cuvalley\plik_export (1).csv')

h['Time'] = pd.to_datetime(h['Time'])
WOS_174L = h.loc[h['file_name'] == r'WOS___174L.csv']
WOS_175L = h.loc[h['file_name'] == r'WOS___175L.csv']
WOS_176L = h.loc[h['file_name'] == r'WOS___176L.csv']
WOS_177L = h.loc[h['file_name'] == r'WOS___177L.csv']
WOS_179L = h.loc[h['file_name'] == r'WOS___179L.csv']