from collections import Counter
from common.classification_metrics_utils import get_classification_metrics
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from os.path import join
from os import walk
from sklearn.decomposition import PCA
from sklearn.metrics import confusion_matrix
# from umap import UMAP
from xgboost import XGBClassifier
from sklearn.model_selection import train_test_split
from catboost import CatBoostClassifier
from imblearn.under_sampling import RandomUnderSampler, ClusterCentroids, TomekLinks


main_folder = r'C:\Users\pawso\Desktop\analysis\LearningCenter\Hackhatons\repo2\cuvalley-hack\SMG\csv'

#### read temp files

sub_folder = join(main_folder, 'statystyki temperatura')
file_in_sub_folder = list(walk(sub_folder))
file_in_sub_folder[0][2]  # list with files names

full_frame_temp_pow = pd.DataFrame()
full_frame_temp_pon = pd.DataFrame()
for file_name in file_in_sub_folder[0][2]:
    file_path = join(sub_folder, file_name)
    temp_frame = pd.read_csv(file_path)
    temp_frame['file_name'] = file_name
    if 'below' in file_name:
        full_frame_temp_pon = pd.concat([full_frame_temp_pon, temp_frame])
    else:
        full_frame_temp_pow = pd.concat([full_frame_temp_pow, temp_frame])

    del temp_frame

full_frame_temp_pow.shape
# full_frame_temp_pow['file_name'].str.startswith('WOS___174L')
full_frame_temp_pow['temp_pow'] = full_frame_temp_pow['val1']
full_frame_temp_pow.head()
full_frame_temp_pon.shape
full_frame_temp_pon['temp_pon'] = full_frame_temp_pon['val1']
full_frame_temp_pon.head()


sub_folder = join(main_folder, 'WOS - skrzynie biegów 2')
file_in_sub_folder = list(walk(sub_folder))
file_in_sub_folder[0][2]  # list with files names

full_frame = pd.DataFrame()
for file_name in file_in_sub_folder[0][2]:
    temp_pow = full_frame_temp_pow[full_frame_temp_pow['file_name'].str.startswith(file_name.replace('.csv', ''))][['Time', 'temp_pow']]
    temp_pon = full_frame_temp_pon[full_frame_temp_pon['file_name'].str.startswith(file_name.replace('.csv', ''))][['Time', 'temp_pon']]
    file_path = join(sub_folder, file_name)
    temp_frame = pd.read_csv(file_path)
    temp_frame = temp_frame.merge(temp_pow, on='Time', how='left')
    temp_frame = temp_frame.merge(temp_pon, on='Time', how='left')
    temp_frame.drop(columns=['Unnamed: 0'], inplace=True)
    temp_frame['file_name'] = file_name
    full_frame = pd.concat([full_frame, temp_frame])
    del temp_frame

full_frame.columns

# deleting 'Time'
time = full_frame['Time'].copy()
file_name = full_frame['file_name'].copy()
full_frame.drop(columns=['Time', 'file_name'], inplace=True)
full_frame.shape
full_frame.columns
full_frame['failure'].value_counts()


## Visualize data with dimension reduction techniques
# PCA
# pca = PCA()
# pca.fit(full_frame.drop(columns=['failure']))
# pca.explained_variance_ratio_[:2].sum()  # w 3 wymiarach będzie elbow method 2 daja 83 a 3 prawie 100%
# pca_frame = pd.DataFrame(PCA(n_components=2).fit_transform(full_frame.drop(columns=['failure'])), columns=['C1', 'C2'])
# pca_frame['failure'] = list(full_frame['failure'].values)
# sns.scatterplot(data=pca_frame, x="C1", y="C2", hue="failure")
#
# # UMAP
# umap = UMAP(n_components=2)
# umap_frame = pd.DataFrame(umap.fit_transform(full_frame.drop(columns=['failure'])), columns=['U1', 'U2'])
# umap_frame['failure'] = list(full_frame['failure'].values)
# sns.scatterplot(data=umap_frame, x="U1", y="U2", hue="failure")

### XGBOOST CLASSIFIER
# here is nice description of this issue: https://machinelearningmastery.com/xgboost-for-imbalanced-classification/
# print(class_ratio)
X = full_frame.drop(columns=['failure']).values
y = full_frame['failure'].values
X.shape
full_frame.shape
######
train_x, test_x, train_y, test_y = train_test_split(X, y, test_size=0.3)
train_x.shape
Counter(train_y)
Counter(test_y)
# train_x, train_y = ClusterCentroids().fit_resample(train_x, train_y)
train_x, train_y = RandomUnderSampler(sampling_strategy=0.8).fit_resample(train_x, train_y)
Counter(train_y)
class_ratio = Counter(train_y)[0] / Counter(train_y)[1]

### ANOMALY DETECTION - unsupervised learning
outliers_fraction = full_frame[full_frame['failure'] == 1].shape[0] / full_frame.shape[0]
clf = IsolationForest(contamination=outliers_fraction)
clf.fit(full_frame.drop(columns=['failure']))
predictions = clf.predict(full_frame.drop(columns=['failure']))
Counter(predictions)
full_frame[['failure']].head()
predictions_standardized = [1 if prediction == -1 else 0 for prediction in predictions]
Counter(predictions_standardized)
get_classification_metrics(full_frame['failure'].values, predictions_standardized)


# xgboost
train_x.shape
xgb_weighted = XGBClassifier(scale_pos_weight=class_ratio).fit(train_x, train_y)
xgb_weighted_predicted = xgb_weighted.predict(test_x)
xgb_weighted_test_probas = xgb_weighted.predict_proba(test_x)[:, 1]
Counter(xgb_weighted_predicted)
Counter(test_y)
get_classification_metrics(test_y, xgb_weighted_predicted)

# PLOT FEATURE IMPORTANCE
col_names = [col_name for col_name in full_frame.columns if col_name != 'failure']
plt.bar(col_names, xgb_weighted.feature_importances_)
plt.bar()

# catboost
cat_weighted = CatBoostClassifier(scale_pos_weight=class_ratio).fit(train_x, train_y)
cat_weighted_predicted = cat_weighted.predict(test_x)
Counter(full_frame['failure'])
Counter(cat_weighted_predicted)
get_classification_metrics(test_y, cat_weighted_predicted)


all_predictions = xgb_weighted.predict_proba(full_frame.drop(columns=['failure']).values)
export = pd.DataFrame(time)
export['predictions'] = all_predictions[:, 1]
export['file_name'] = file_name
export.head()
export.to_csv(r'C:\Users\pawso\Desktop\analysis\LearningCenter\Hackhatons\plik_export.csv')