from sklearn.metrics import confusion_matrix


def get_classification_metrics(y_real, y_pred):
    cm = confusion_matrix(y_real, y_pred)
    print('Confusion Matrix : \n', cm)
    tp = cm[0, 0]
    tn = cm[1, 1]
    fn = cm[0, 1]
    fp = cm[1, 0]

    accuracy = (tp + tn) / cm.sum()
    print(f'Accuracy: {round(accuracy, 4)}.')

    sensitivity = tp / (tp + fn)
    print(f'Sensitivity : {round(sensitivity, 4)}.')

    specificity = tn / (tn + fp)
    print(f'Specificity: {round(specificity, 4)}.')

    mean_sensitivity_and_specificity = 1/2 * (sensitivity + specificity)
    print(f'Mean of sensitivity and specificity: {round(mean_sensitivity_and_specificity, 4)}.')

    return {'accuracy': accuracy, 'sensitivity': sensitivity, 'specificity': specificity,
            'mean_sensitivity_and_specificity': mean_sensitivity_and_specificity}
