import random

import pandas as pd
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import Dense, Dropout, Flatten, BatchNormalization, Activation
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.models import Sequential

from Furnace.data_collector import get_frame
from Furnace.data_generator import DataGenerator, prepare_data

list_of_frames = get_frame()
params = {
    'dim': (60 * 20, 26),
    'batch_size': 1000,
    'lag': 60 * 5,
    'shuffle': False
}

total_frame, values, list_of_ids = prepare_data(list_of_frames, params['dim'], params['lag'])
total_frame = pd.DataFrame(StandardScaler().fit_transform(total_frame))

# Datasets - hardcoded for debugging purposes
train_ids = list(list_of_ids[0:10000])
validation_ids = list(list_of_ids[10000:20000])
random.shuffle(train_ids)
# validation_ids = validation_ids[:10000]
partition = {'train': train_ids, 'validation': validation_ids}

# Generators
training_generator = DataGenerator(total_frame, values, partition['train'], **params)
validation_generator = DataGenerator(total_frame, values, partition['validation'], **params)

model = Sequential()
model.add(Conv1D(filters=32, kernel_size=50, activation='relu', input_shape=(params['dim'][0], params['dim'][1])))
model.add(BatchNormalization())
model.add(MaxPooling1D(pool_size=2))
model.add(Conv1D(filters=32, kernel_size=50, activation='relu'))
model.add(BatchNormalization())
model.add(Dropout(0.1))
model.add(MaxPooling1D(pool_size=2))
model.add(Flatten())
model.add(Dense(128, input_dim=60 * 5 * 5))
model.add(Activation('sigmoid'))
model.add(Dense(1000, activation='relu'))
model.add(Dense(1000, activation='relu'))
model.add(Dense(128, activation='relu'))
model.add(Dense(1, activation='relu'))
model.compile(loss='mean_squared_error', optimizer='adam')

model.summary()

model.fit_generator(generator=training_generator,
                    validation_data=validation_generator,
                    epochs=2)

model.save("C:/Users/Arristrasz/Desktop/Projekty/CuValley Hackaton/Materiały/prediction_model.h5")
