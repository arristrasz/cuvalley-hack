"""
    * This script requires specifying model's path
"""

import random

import pandas as pd
from matplotlib import pyplot as plt
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.models import load_model

from Furnace.data_collector import get_frame
from Furnace.data_generator import DataGenerator, prepare_data

model = load_model("C:/Users/Arristrasz/Desktop/Projekty/CuValley Hackaton/Materiały/model.h5")

list_of_frames = get_frame()
params = {
    'dim': (60 * 20, 5),  # tutaj kolejno: liczba obserwacji (czas), liczba zmiennych
    'batch_size': 1000,
    'lag': 60 * 5,  # tutaj o ile lagujemy Yrka względem obserwacji
    'shuffle': False  # everyday i'm shuffling
}

total_frame, values, list_of_ids = prepare_data(list_of_frames, params['dim'], params['lag'])
total_frame = total_frame[['PPD', 'ZTWD', 'PD', 'NPP', 'STRATA_ALL']]
total_frame = pd.DataFrame(StandardScaler().fit_transform(total_frame))

train_ids = list(list_of_ids[0:20000])
validation_ids = list(list_of_ids[0:20000])
random.shuffle(train_ids)

partition = {'train': train_ids, 'validation': validation_ids}

# Generators
training_generator = DataGenerator(total_frame, values, partition['train'], **params)
validation_generator = DataGenerator(total_frame, values, partition['validation'], **params)

test = model.predict_generator(validation_generator)
timestamp = [i / 60 for i in range(20000)]
plt.plot(timestamp, values[60 * 5 + 60 * 20:20000 + 60 * 5 + 60 * 20], label='Oryginalny przebieg strat')
plt.plot(timestamp, [value[0] for value in test], label='Predykcja przebiegu strat (5 min)')
plt.xlabel('Czas [min]')
plt.ylabel('Straty [MW]')
plt.legend()
plt.show()
print(1)
