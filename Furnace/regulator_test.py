"""
    * This script requires specifying model's path
"""

import random

import pandas as pd
from matplotlib import pyplot as plt
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.models import load_model
from Furnace.controlling_management import ControllingManager


import numpy as np
from Furnace.data_collector import get_frame
from Furnace.data_generator import DataGenerator, prepare_data

model = load_model("C:/Users/Arristrasz/Desktop/Projekty/CuValley Hackaton/Materiały/model.h5")

list_of_frames = get_frame()
params = {
    'dim': (60 * 20, 5),  # tutaj kolejno: liczba obserwacji (czas), liczba zmiennych
    'batch_size': 1000,
    'lag': 60 * 5,  # tutaj o ile lagujemy Yrka względem obserwacji
    'shuffle': False  # everyday i'm shuffling
}

total_frame, values, list_of_ids = prepare_data(list_of_frames, params['dim'], params['lag'])
total_frame = total_frame[['PPD', 'ZTWD', 'PD', 'NPP', 'STRATA_ALL']]
# total_frame = pd.DataFrame(StandardScaler().fit_transform(total_frame))

original_ppd = total_frame['PPD'][:]
original_ppd = [item for item in original_ppd]

original_strata_all = total_frame['STRATA_ALL'][:]
original_strata_all = [item for item in original_strata_all]

manager = ControllingManager()
for row_id in range(22000):
    actual_row = row_id + 60 * 20
    prediction_row = actual_row + 60 * 5
    obs = total_frame[(total_frame.index > actual_row - params['dim'][0]) & (total_frame.index <= actual_row)].values
    obs = StandardScaler().fit_transform(obs)
    prediction = model.predict(obs.reshape((1, obs.shape[0], obs.shape[1])))[0][0]
    new_ppd, new_ztwd, new_pd, new_nnp = manager(prediction,
                                                 current_ppd=total_frame['PPD'][actual_row-1],
                                                 current_ztwd=total_frame['ZTWD'][actual_row-1],
                                                 current_pd=total_frame['PD'][actual_row-1],
                                                 current_nnp=total_frame['NPP'][actual_row-1])
    total_frame['PPD'][actual_row] = new_ppd
    total_frame['ZTWD'][actual_row] = new_ztwd
    total_frame['PD'][actual_row] = new_pd
    total_frame['NPP'][actual_row] = new_nnp
    # total_frame['STRATA_ALL'][prediction_row] = prediction

calculated_ppd = total_frame['PPD']
calculated_ppd = [item for item in calculated_ppd]


plt.plot(original_ppd, label='Oryginalnie sterowane aeracją (sternik) [Nm3/h]')
plt.plot(calculated_ppd, label='Predykcyjnie sterowane aeracją [Nm3/h]')
# plt.plot(original_strata_all, label='Zamrożona strata [MW]')
plt.xlabel('Czas [min]')
plt.ylabel('Straty [MW]')
plt.legend()
plt.show()
print(1)
