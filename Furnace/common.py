# Define which variables should be used for controlling (one or many)
USE_PPD_CONTROLLER = True
USE_ZTWD_CONTROLLER = True
USE_PD_CONTROLLER = True
USE_NNP_CONTROLLER = True

# Define variable ranges and max change (min, max, max_change_per_time_unit, time_unit [s])
PPD_MIN, PPD_MAX, PPD_DELTA, PPD_DELTA_T = 1900, 3500, 80, 1
ZTWD_MIN, ZTWD_MAX, ZTWD_DELTA, ZTWD_DELTA_T = 65, 81, 0.8, 60
PD_MIN, PD_MAX, PD_DELTA, PD_DELTA_T = 40, 70, 2, 1
NNP_MIN, NNP_MAX, NNP_DELTA, NNP_DELTA_T = 13, 27, 14, 5 * 60

# Define PIC parameters for controlling the output
# If a variable is inversely proportional to controlled value assign minus sign to its KP, KI and KD
PID_PARAMS = {
    'SETPOINT': 23,  # MW
    'PPD_KP': 1, 'PPD_KI': 0.5, 'PPD_KD': 0.3, 'PPD_MIN': PPD_MIN, 'PPD_MAX': PPD_MAX,
    'ZTWD_KP': -1, 'ZTWD_KI': 0.1, 'ZTWD_KD': 0.05, 'ZTWD_MIN': ZTWD_MIN, 'ZTWD_MAX': ZTWD_MAX,
    'PD_KP': 1, 'PD_KI': 0.1, 'PD_KD': 0.05, 'PD_MIN': PD_MIN, 'PD_MAX': PD_MAX,
    'NNP_KP': -1, 'NNP_KI': -0.1, 'NNP_KD': -0.05, 'NNP_MIN': NNP_MIN, 'NNP_MAX': NNP_MAX,
}
