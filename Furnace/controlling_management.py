from simple_pid import PID
from Furnace.common import (PID_PARAMS, PPD_DELTA, PPD_DELTA_T, ZTWD_DELTA,
                            ZTWD_DELTA_T, PD_DELTA, PD_DELTA_T, NNP_DELTA, NNP_DELTA_T,
                            USE_PPD_CONTROLLER, USE_ZTWD_CONTROLLER, USE_PD_CONTROLLER, USE_NNP_CONTROLLER)


class ControllingManager(object):
    """Manager used for updating 'manipulowane' variables based on provided constraints."""

    use_ppd = USE_PPD_CONTROLLER
    use_ztwd = USE_ZTWD_CONTROLLER
    use_pd = USE_PD_CONTROLLER
    use_nnp = USE_NNP_CONTROLLER

    # Elapsed time since last value change
    _time_elapsed_ppd = 0
    _time_elapsed_ztwd = 0
    _time_elapsed_pd = 0
    _time_elapsed_nnp = 0

    controller_ppd = PID(PID_PARAMS['PPD_KP'], PID_PARAMS['PPD_KI'], PID_PARAMS['PPD_KD'],
                         setpoint=PID_PARAMS['SETPOINT'],
                         sample_time=None, output_limits=(PID_PARAMS['PPD_MIN'], PID_PARAMS['PPD_MAX']),
                         proportional_on_measurement=True)

    controller_ztwd = PID(PID_PARAMS['ZTWD_KP'], PID_PARAMS['ZTWD_KI'], PID_PARAMS['ZTWD_KD'],
                          setpoint=PID_PARAMS['SETPOINT'], sample_time=None,
                          output_limits=(PID_PARAMS['ZTWD_MIN'], PID_PARAMS['ZTWD_MAX']),
                          proportional_on_measurement=True)

    controller_pd = PID(PID_PARAMS['PD_KP'], PID_PARAMS['PD_KI'], PID_PARAMS['PD_KD'], setpoint=PID_PARAMS['SETPOINT'],
                        sample_time=None, output_limits=(PID_PARAMS['PD_MIN'], PID_PARAMS['PD_MAX']),
                        proportional_on_measurement=True)

    controller_nnp = PID(PID_PARAMS['NNP_KP'], PID_PARAMS['NNP_KI'], PID_PARAMS['NNP_KD'],
                         setpoint=PID_PARAMS['SETPOINT'],
                         sample_time=None, output_limits=(PID_PARAMS['NNP_MIN'], PID_PARAMS['NNP_MAX']),
                         proportional_on_measurement=True)

    def __init__(self):
        pass

    def manage_controlling_variables(self, controlled_variable, current_ppd, current_ztwd, current_pd, current_nnp):
        pass

    def __call__(self, controlled_variable, current_ppd, current_ztwd, current_pd, current_nnp):

        # Update PID controllers:
        desired_ppd_value = self.controller_ppd(controlled_variable)
        desired_ztwd_value = self.controller_ztwd(controlled_variable)
        desired_pd_value = self.controller_pd(controlled_variable)
        desired_nnp_value = self.controller_nnp(controlled_variable)

        self._increment_elapsed_time()

        if self.use_ppd:
            if self._time_elapsed_ppd >= PPD_DELTA_T:
                new_ppd = max(min(desired_ppd_value, current_ppd + PPD_DELTA), current_ppd - PPD_DELTA)
                self._time_elapsed_ppd = 0
            else:
                new_ppd = current_ppd
        else:
            new_ppd = None

        if self.use_ztwd:
            if self._time_elapsed_ztwd >= ZTWD_DELTA_T:
                new_ztwd = max(min(desired_ztwd_value, current_ztwd + ZTWD_DELTA), current_ztwd - ZTWD_DELTA)
                self._time_elapsed_ztwd = 0
            else:
                new_ztwd = current_ztwd
        else:
            new_ztwd = None

        if self.use_pd:
            if self._time_elapsed_ppd >= PD_DELTA_T:
                new_pd = max(min(desired_pd_value, current_pd + PD_DELTA), current_pd - PD_DELTA)
                self._time_elapsed_pd = 0
            else:
                new_pd = current_pd
        else:
            new_pd = None

        if self.use_nnp:
            if self._time_elapsed_nnp >= NNP_DELTA_T:
                new_nnp = max(min(desired_nnp_value, current_nnp + NNP_DELTA), current_nnp - NNP_DELTA)
                self._time_elapsed_nnp = 0
            else:
                new_nnp = current_nnp
        else:
            new_nnp = None

        return new_ppd, new_ztwd, new_pd, new_nnp

    def _increment_elapsed_time(self):
        self._time_elapsed_ppd += 1
        self._time_elapsed_ztwd += 1
        self._time_elapsed_pd += 1
        self._time_elapsed_nnp += 1
