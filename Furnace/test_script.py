from Furnace.controlling_management import ControllingManager
from math import sin
from matplotlib import pyplot as plt

# Assume we have a system we want to control in controlled_system
# v = controlled_system.update(0)

manager = ControllingManager()


def waveform(value):
    return sin(value / 10) + 22 * 1000 / (value + 100) + 10 * sin(value / 20) + value / 100


control_ppd = []
control_ztwd = []
control_pd = []
control_nnp = []
ppd, ztwd, pd, nnp = 2500, 75, 50, 20
for i in range(1000):
    # Compute new output from the PID according to the systems current value
    new_ppd, new_ztwd, new_pd, new_nnp = manager(waveform(i), current_ppd=ppd, current_ztwd=ztwd, current_pd=pd,
                                                 current_nnp=nnp)
    if new_ppd:
        ppd = new_ppd
    if new_ztwd:
        ztwd = new_ztwd
    if new_pd:
        pd = new_pd
    if new_nnp:
        nnp = new_nnp

    control_ppd.append(ppd)
    control_ztwd.append(ztwd)
    control_pd.append(pd)
    control_nnp.append(nnp)

plt.plot(list(range(1000)), control_ppd)
plt.plot(list(range(1000)), control_ztwd)
plt.plot(list(range(1000)), control_pd)
plt.plot(list(range(1000)), control_nnp)
plt.plot(list(range(1000)), [waveform(i) for i in range(1000)])
plt.show()
print(1)
