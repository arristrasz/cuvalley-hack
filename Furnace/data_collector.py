import pandas as pd
import os


def get_frame():
    frame_list = []
    for root, dirs, files in os.walk(r"C:/Users/Arristrasz/Desktop/Projekty/CuValley Hackaton/Materiały/dane_piec/Piec HMG/hmg - straty pz/", topdown=False):
        for name in files:
            if 'manipulowane' in name:
                manipulowane = pd.read_csv(os.path.join(root, name))
                straty = pd.read_csv(os.path.join(root, 'straty' + name[len('manipulowane'):]))
                zaklocajace = pd.read_csv(os.path.join(root, 'zaklocajace' + name[len('manipulowane'):]))
                zaklocane = pd.read_csv(os.path.join(root, 'zaklocane' + name[len('manipulowane'):]))

                manipulowane.drop(columns='Unnamed: 5', inplace=True)
                manipulowane.rename(columns={'001FCx00285_SPPV.PV': 'PPD', '001XXXCALC01.NUM.PV[3]': 'ZTWD',
                                             '001SCx00274_SPPV.PV': 'PD', '001FCx00241_sppv.pv': 'NPP'}, inplace=True)

                straty.rename(columns={'001NIR0SZR0.daca.pv': 'STRATA_ALL', '001NIR0SZRG.daca.pv': 'STRATA_G',
                                       '001NIR0S600.daca.pv': 'STRATA_600', '001NIR0S500.daca.pv': 'STRATA_500',
                                       '001NIR0S300.daca.pv': 'STRATA_300', '001NIR0S100.daca.pv': 'STRATA_100',
                                       }, inplace=True)
                straty.drop(columns='czas', inplace=True)

                zaklocajace.rename(columns={'001FYx00206_SPSUM.pv': 'ZAK_0', '001FCx00231_SPPV.PV': 'ZAK_1',
                                            '001FCx00251_SPPV.PV': 'ZAK_2', '001FCx00281.PV': 'ZAK_3',
                                            '001FCx00262.PV': 'ZAK_4', '001FCx00261.PV': 'ZAK_5',
                                            '001XXXCALC01.NUM.PV[2]': 'ZAK_6', 'prob_corg': 'ZAK_7',
                                            'prob_s': 'ZAK_8', 'sita_nadziarno': 'ZAK_9',
                                            'sita_podziarno': 'ZAK_10', 'poziom_zuzel': 'ZAK_11',
                                            }, inplace=True)
                zaklocajace.drop(columns='Czas', inplace=True)

                zaklocane.rename(columns={'001UCx00274.pv': 'STATE_0', '001NIR0ODS0.daca.pv': 'STATE_1',
                                          'temp_zuz': 'STATE_2', '007SxR00555.daca1.pv': 'STATE_3'
                                          }, inplace=True)
                zaklocane.drop(columns='Unnamed: 5', inplace=True)
                zaklocane.drop(columns='Czas', inplace=True)

                combined_frame = pd.concat([manipulowane, straty, zaklocajace, zaklocane], axis=1)
                frame_list.append(combined_frame)

    return [frame_list[7]]
