from tqdm import tqdm
import numpy as np
import pandas as pd
from tensorflow.keras.utils import Sequence


class DataGenerator(Sequence):
    def __init__(self, frame: pd.DataFrame, values: np.ndarray, list_of_ids: list, batch_size: int, dim: tuple,
                 lag: int, shuffle=True):
        self.frame = frame  # nasza ramka bez ygreków
        self.values = values  # nasze ygreki
        self.list_of_ids = list_of_ids
        self.batch_size = batch_size
        self.dim = dim
        self.lag = lag
        self.shuffle = shuffle

        self.on_epoch_end()

    def __len__(self):
        # tutaj zwracamy liczbę batchy na epoch
        return int(np.floor(len(self.list_of_ids) / self.batch_size))

    def __getitem__(self, index):
        # generujemy indeksy batcha
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # bierzemy listę idików
        list_of_ids_temp = [self.list_of_ids[k] for k in indexes]

        return self._generate_batch_data(list_of_ids_temp)

    def on_epoch_end(self):
        # updatuje indeksy po każdym epochu
        self.indexes = np.arange(len(self.list_of_ids))
        if self.shuffle:
            np.random.shuffle(self.indexes)

    def _generate_batch_data(self, list_of_ids_temp):
        # tworzymy puste ramki o wymaganych finalnych wymiarach
        X = np.empty((self.batch_size, *self.dim))
        y = np.empty(self.batch_size, dtype=float)

        # tworzymy obserwacje w pętli
        for index, row_id in enumerate(list_of_ids_temp):
            obs = self.frame[(self.frame.index > row_id - self.dim[0]) & (self.frame.index <= row_id)].values
            # obs = np.transpose(obs)
            X[index, ] = obs.reshape((obs.shape[0], obs.shape[1]))
            # obs.shape
            y[index] = self.values[row_id + self.lag]

        return X, y


def prepare_data(list_of_frames, dim, lag):
    total_frame = pd.DataFrame()
    for temp_frame in tqdm(list_of_frames):
        temp_frame.reset_index(inplace=True, drop=True)
        temp_frame['is_used'] = (temp_frame.index >= dim[0]) & \
                                (temp_frame.index < temp_frame.shape[0] - lag)
        total_frame = pd.concat([total_frame, temp_frame])

    total_frame.reset_index(inplace=True, drop=True)
    values = total_frame['STRATA_ALL'].values
    list_IDs = total_frame[total_frame['is_used'].isin([True])].index  # przypisujemy id z których możemy korzystać

    total_frame.drop(columns=['is_used', 'Czas'], inplace=True)  # drop już niepotrzebnej kolumny
    return total_frame, values, list_IDs
